FROM ubuntu as opfsbuilder
WORKDIR /root/
RUN apt-get update \
 && apt-get install -y git build-essential \
 && git clone https://github.com/titech-os/opfs.git \
 && (cd opfs; make PREFIX=/root/local install)

FROM ubuntu

LABEL maintainer="mlentz@cs.duke.edu"

ARG TZ=EDT
ARG USER=student
ARG GROUP=student
ARG PASS=student
ENV HOME=/home/${USER}
ENV WORKDIR=${HOME}/labs

COPY --from=opfsbuilder /root/local/bin/* /usr/local/bin/

RUN apt-get update \
 && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends tzdata \
 && ln -sf /usr/share/zoneinfo/${TZ} /etc/localtime \
 && dpkg-reconfigure --frontend noninteractive tzdata \
 && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
      sudo \
      git \
      build-essential \
      gdb-multiarch \
      qemu-system-misc \
      gcc-riscv64-unknown-elf \
      binutils-riscv64-unknown-elf \
      python3 \
 && rm -rf /var/lib/apt/lists/* \
 && groupadd ${GROUP} \
 && useradd -g ${GROUP} -m ${USER} \
 && (echo "${USER}:${PASS}" | chpasswd) \
 && gpasswd -a ${USER} sudo \
 && mkdir -p ${WORKDIR} \
 && chown -R ${USER}:${GROUP} ${WORKDIR} \
 && (echo "add-auto-load-safe-path ${WORKDIR}/.gdbinit" > ${HOME}/.gdbinit) \
 && chown ${USER}:${GROUP} ${HOME}/.gdbinit

USER ${USER}
WORKDIR ${WORKDIR}

ENTRYPOINT ["/bin/bash"]
