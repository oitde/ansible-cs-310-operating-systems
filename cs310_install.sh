#!/bin/bash

export DEBIAN_FRONTEND=noninteractive

sudo apt-get update
sudo apt-get autoremove -y
sudo apt-get install -y --no-install-recommends \
    software-properties-common \
    wget \
    pwgen \
    net-tools \
    build-essential \
    git \
    bzip2 \
    vim \
    unzip 

# install docker prereqs
sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
# install docker repo
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/docker-archive-keyring.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/trusted.gpg.d/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list
# install docker packages
sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io

# tell docker to build the image
sudo docker build --tag cs310 .

